﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HierarchyHW3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AchievPage : ContentPage
    {
        Uri uri;
        private bool pageToWeb = false;
        public AchievPage()
        {
            InitializeComponent();
        }

        // Appear/Disappear Event
        protected override void OnAppearing()
        {
            if (pageToWeb == false)
                DisplayAlert("Alert", "Welcome to the Achievements page!", "OK");
            pageToWeb = false;
            base.OnAppearing();
        }
        protected override void OnDisappearing()
        {
            if (pageToWeb == false)
                DisplayAlert("Alert", "Download WoW Challenger addon for more features!", "OK");
            base.OnDisappearing();
        }
        // -------

        // Go on Website Event
        public void btnBrowse_Click(object sender, System.EventArgs e)
        {
            string data = ((MenuItem)sender).BindingContext as string;
            go_url(data);
        }
        async Task go_url(String url)
        {
            pageToWeb = true;
            uri = new Uri(url);
            await Browser.OpenAsync(uri, BrowserLaunchMode.SystemPreferred);
        }
        // -------
    }
}