﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HierarchyHW3
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void GoToAchievPage(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AchievPage());
        }

        async void GoToMountsPage(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MountsPage());
        }

        async void GoToPetsPage(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PetsPage());
        }
    }
}
